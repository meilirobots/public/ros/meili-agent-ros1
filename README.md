# Meili agent

## Prerequisites. 

1. Install common dependencies

```
sudo apt-get update -y
sudo apt-get install git
sudo apt-get install python3-pip
sudo -H pip3 install --upgrade pip
```

2. Catkin:


For ROS Melodic:

```
sudo apt-get install ros-melodic-catkin
```

For ROS noetic:

```
sudo apt-get install ros-noetic-catkin
```

3. Rosdep

```
sudo apt-get install python3-rosdep
``` 

4. Download meili-cli tool to clone the repository linked to a fleet setup: 
```
curl https://meili-fms-dev.s3.eu-north-1.amazonaws.com/helpers/meili-cli-386 --output meili-cli
chmod +x meili-cli
```

## Installation

1. Create a ROS workspace if not created
```
mkdir -p ~/catkin_ws_meili/src
cd ~/catkin_ws_meili
catkin_make
```

2. Save ROS_PACKAGE_PATH environment variable to clone later the meili agent. 
```
export LAST_CATKIN_PATH=`pwd`'/src'
cd $THISPATH
export ROS_PACKAGE_PATH=$LAST_CATKIN_PATH:$ROS_PACKAGE_PATH
```

3. Generate a fleet setup in Meili FMS and get the PIN by following this tutorial: https://docs.meilirobots.com/docs/get-started/add-vehicles/ros-setup/

This repository can be cloned by 2 different ways. Both ways are shown in the following points 4 and 5. Only one should be run.

4. Clone meili-agent by running meili-cli tool with the PIN generated previously:
```
./meili-cli init -pin PIN -site SITE
./meili-cli setup
```

5. Clone meili agent by clonning this repository: 
```
git clone https://gitlab.com/meilirobots/public/ros/meili-agent-ros1.git
``` 

No matter how the meili agent has been installed (4 or 5), continue with the installation. 

6. Install python packages: 
```
cd ~/catkin_ws_meili/src/meili-agent/src/
sudo -H pip3 install --ignore-installed PyYAML
sudo -H pip3 install -r requirements.txt
sudo apt-get update
```

7. Update rosdep and install Meili agent ros dependencies: 
```
cd ~/catkin_ws_meili/
#Change noetic by melodic if using melodic
source /opt/ros/noetic/setup.bash 
source ~/catkin_ws_meili/devel/setup.bash
```

8. Build the workspace: 
```
catkin_make
source ~/catkin_ws_meili/devel/setup.bash
```

In case the meili agent has been installed following point 5 instead of 4, you have to setup the config file in which the vehicles fleet is described. (When installing the meili agent following point 4,  this config file is generated automatically). Set up config file: 

9. Create config.yaml file inside config folder
```
cd ~/catkin_ws_meili/src/meili-agent/config
touch config.yaml
```

10. Config file contain information about the fleet, specific vehicles and MQTT auth. This information need to be configured using Meili Cloud. An example of config file:

```
fleet: df1d4b9df56fe55fc43e34e16e533d3df050fa86
vehicles:
-	uuid: bae59388be704a6e89e5c8a7907b74243
	token:71876edfe6ea939e696f85158c0c7516187136e94
	prefix:
mqqt_id: ""
influx_details:
token: null
host: null
variables: {}
```

11. Rebuild ROS workspace:
```
cd catkin_ws_meili
catkin_make
source ~/catkin_ws_meili/devel/setup.bash
```

## Parameters
- publish_frequency: Please modify the publication frequency to suit your needs, with a minimum value of 1 Hz.
```
export publish_frequency=1
```

- battery_present: Indicates if battery values are available and if the user wants to stream it to the cloud. Values can be True or False. 
```
export battery_present=False
```

- path_planning: When allocating tasks with multiple points, the user can choose to use a route calculated by Meili FMS’s path planner. True or False. 
```
export path_planning=True
```

- traffic_control: Indicates if real-time paths calculated by AMRs are streamed to the cloud, and traffic control algorithms will be applied.
    - True: Traffic control algorithms will be applied when more than one task is on.
    - False: Default value. No path will be streamed and no traffic control algorithms.
```
export traffic_control=True
```

- offline: Indicates if offline mode is activated in the agent.
    - true:  The Meili Agent will periodically check the wifi connectivity. 
    - false:  Default value. Wifi connection will not be checked periodically. 
```
export offline=False
```

- lipo_cells: Indicates whether the battery is a LiPo battery or not. If >0 the battery is a LiPo battery.
```
export lipo_cells=0
```

## Running
If you want to launch the meili agent with the default parameters (the ones defined in the previous section), run the following command:

```
roslaunch meili_agent meili_agent.launch
```

In case some parameters want to be changed, define the parameter as an environment variable as explained in the previous section and run the meili agent including the variables desired:
```
roslaunch meili_agent meili_agent.launch parameter1:=$parameter1 parameter2=:parameter2
```
Where parameter1 and parameter2 are the name of the variables defined in the previous section. You can include in the ros2 launch command as many parameters as you need.



## Formatting

1. Install pre-commit from https://pre-commit.com/

2. Run `pre-commit install`

3. Happy committing!

## For WAYPOINTS

WAYPOINTS in ROS functions using a STATE MACHINE that presents three states:

- GetPath: where it is checked that the waypoints[] is not empty. If it is not empty the path is ready.

- FollowPath: an Action Client is created for the MoveBaseAction server. This server will depend on the vehicle prefix. For example: /robot0/move_base or /robot1/move_base.
Each goal of the waypoints array is sent until finalized.

-PathComplete: once all the goals have been reached the PathComplete state starts. This state ends the state machine.

Additional functionalities to cancel or reset the waypoints are set, however they are not used. Maybe for the future some will be interesting to be implemented to add complexity to the state machine decision making,

To run the waypoints, it is important that the following section is uncomment in the file `ws.py`. This allows a fake_waypoint_generator to include two additional goals with respect to the original task, generating an array of goals.


```shell
################# UNCOMMENT THIS LINES WHEN WANTING TO TEST WAYPOINTS #################
x_w, y_w, xm_w, ym_w, rotation_w = self.fake_waypoints_generator(x, y, xm, ym, rotation)

self.waypoints_handler(
    vehicle=vehicle, x=x_w, y=y_w, xm=xm_w, ym=ym_w, rotation=rotation_w

)
########################################################################################
            
```

Finally, run the agent like normal and you will see the following output:
```shell
roslaunch turtlebot3_gazebo turtlebot3_house.launch
roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=/home/.../maps/map-full.yaml
roslaunch meili_agent meili_agent.launch             
```

OUTPUT:
```shell
[INFO] [1628159411.714377, 60.477000]: [ROSAgent] Topic /move_base is configure for vehicle 11dd669722734298b6f5242c48e7c6d6
[ DEBUG ] : Adding state (GET_PATH, <follow_waypoints.GetPath object at 0x7f47127e0a00>, {'success': 'FOLLOW_PATH'})
[ DEBUG ] : Adding state 'GET_PATH' to the state machine.
[ DEBUG ] : State 'GET_PATH' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR GET_PATH: {'success': 'FOLLOW_PATH'}
[ DEBUG ] : Adding state (FOLLOW_PATH, <follow_waypoints.FollowPath object at 0x7f47127e06d0>, {'success': 'PATH_COMPLETE'})
[ DEBUG ] : Adding state 'FOLLOW_PATH' to the state machine.
[ DEBUG ] : State 'FOLLOW_PATH' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR FOLLOW_PATH: {'success': 'PATH_COMPLETE'}
[ DEBUG ] : Adding state (PATH_COMPLETE, <follow_waypoints.PathComplete object at 0x7f47127e0970>, {'success': 'end'})
[ DEBUG ] : Adding state 'PATH_COMPLETE' to the state machine.
[ DEBUG ] : State 'PATH_COMPLETE' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR PATH_COMPLETE: {'success': 'end'}
[  INFO ] : State machine starting in initial state 'GET_PATH' with userdata: 
	['server']
[INFO] [1628159411.732828, 60.491000]: Path Ready
[  INFO ] : State machine transitioning 'GET_PATH':'success'-->'FOLLOW_PATH'
[INFO] [1628159411.769716, 60.519000]: Connecting to /move_base...
[INFO] [1628159411.947589, 60.650000]: Connected to /move_base
[INFO] [1628159411.951432, 60.651000]: Starting a tf listener.
[INFO] [1628159411.973933, 60.658000]: Executing move_base goal to position (x,y): -0.55, 2.5
[INFO] [1628159428.608714, 76.272000]: Waiting for 0.000000 sec...
[INFO] [1628159428.613411, 76.276000]: Executing move_base goal to position (x,y): -0.050000000000000044, 3.0
[INFO] [1628159470.824908, 114.631000]: Waiting for 0.000000 sec...
[INFO] [1628159470.828604, 114.636000]: Executing move_base goal to position (x,y): 0.44999999999999996, 3.5
[INFO] [1628159534.089987, 176.040000]: Waiting for 0.000000 sec...
[  INFO ] : State machine transitioning 'FOLLOW_PATH':'success'-->'PATH_COMPLETE'
[INFO] [1628159534.094006, 176.044000]: ##### REACHED LAST WAYPOINT #####
[  INFO ] : State machine terminating 'PATH_COMPLETE':'success':'end'
             
```


