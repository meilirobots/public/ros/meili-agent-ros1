import rospy
from actionlib_msgs.msg import GoalID
from meili_agent.msg import Topicinit
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped



def parse_goal(x, y, rotation_quaternion):
    # parse goal in ros format for sending navigation goal
    goal = PoseStamped()
    goal.header.stamp = rospy.Time.now()
    goal.header.frame_id = "map"
    goal.pose.position.x = float(x)
    goal.pose.position.y = float(y)
    goal.pose.position.z = 0.0
    goal.pose.orientation.x = float(rotation_quaternion["qx"])
    goal.pose.orientation.y = float(rotation_quaternion["qy"])
    goal.pose.orientation.w = float(rotation_quaternion["qw"])
    goal.pose.orientation.z = float(rotation_quaternion["qz"])
    return goal

def parse_initial_pose(x, y, rotation_quaternion):
    # parse goal in ros format for sending navigation goal
    goal = PoseWithCovarianceStamped()
    goal.header.stamp = rospy.Time.now()
    goal.header.frame_id = "map"
    goal.pose.pose.position.x = float(x)
    goal.pose.pose.position.y = float(y)
    goal.pose.pose.position.z = 0.0
    goal.pose.pose.orientation.x = float(rotation_quaternion["qx"])
    goal.pose.pose.orientation.y = float(rotation_quaternion["qy"])
    goal.pose.pose.orientation.w = float(rotation_quaternion["qw"])
    goal.pose.pose.orientation.z = float(rotation_quaternion["qz"])
    return goal

def parse_cancel_goal(goal_id):
    goal = GoalID()
    goal.stamp = rospy.Time.now()
    goal.id = str(goal_id)
    return goal


def parse_speed_limit(speed_limit):
    speed_limit = {"max_vel_x": speed_limit}
    return speed_limit

def parse_xy_tolerance(xy_tolerance): 
    xy_tolerance = {"xy_goal_tolerance": xy_tolerance}
    return xy_tolerance

def parse_yaw_tolerance(yaw_tolerance): 
    yaw_tolerance = {"yaw_goal_tolerance": yaw_tolerance}
    return yaw_tolerance


def parse_topic(data, agent):
    vehicle_position = agent.return_vehicle_position(data["vehicle_uuid"])

    msg = Topicinit()
    msg.vehicle_token = data["vehicle_uuid"]
    msg.topic_uuid = data["uuid"]
    msg.topic_name = data["topic"]
    msg.topic_type = data["message_type"]

    msg.mqtt_id = agent.mqtt_id
    msg.frequency = int(agent.publish_frequency)

    msg.total_count = agent.total_topics
    msg.number_of_vehicles = vehicle_position + 1
    msg.prefix = agent.vehicles[vehicle_position]["prefix"]

    return msg
