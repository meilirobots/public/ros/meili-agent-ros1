#!/usr/bin/env python3

"""Principal Agent. Initialization of node"""
import logging
# standard python3 libraries
import signal
import sys
# ROS1 dependencies
import rospy

# Meili agent dependencies
from logging_ros import log_info, log_error

from agent import MeiliAgent
from handlers import SDKHandlers
from publishers_subscribers import pub_and_sub
from sentry_sdk import capture_exception

from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.agent_setup import Setup
from meili_ros_lib.connection import Connection

from api_handlers import APIHandlers

initialize_sentry()


class InitiationNode(Setup):
    """Class for creating ROS node."""

    def __init__(self):
        # Node initialization
        rospy.init_node("agent")

        # General Setup
        Setup.__init__(self)

        # ROS1
        # Logging
        self.log_info = log_info
        self.log_error = log_error

        self.init_parameters_setup()
        self.get_launch_parameters()

    def get_launch_parameters(self):
        try:
            # ROS1
            self.path_planning = rospy.get_param("path_planning")
            self.traffic_control = rospy.get_param("traffic_control")
            battery = rospy.get_param("battery_present")
            lipo_cells = rospy.get_param("lipo_cells")
            self.publish_frequency = rospy.get_param("publish_frequency")
            self.offline_flag = rospy.get_param("offline")
            self.logging=rospy.get_param("logging")
            self.v2=rospy.get_param("v2")
            self.cmd_vel_topic=rospy.get_param("cmd_vel_topic")
            self.RTLS=rospy.get_param("RTLS")

            # Intermediate waypoint position and orientation tolerance
            self.xy_intermediate_wayp_tolerance = rospy.get_param("xy_intermediate_wayp_tol")
            self.xy_final_wayp_tolerance = rospy.get_param("xy_final_wayp_tol")
            # Final waypoint position and orientation tolerance
            self.yaw_intermediate_wayp_tolerance = rospy.get_param("yaw_intermediate_wayp_tol")
            self.yaw_final_wayp_tolerance = rospy.get_param("yaw_final_wayp_tol")

            self.battery_parameter(battery, lipo_cells)

        except KeyError:
            self.log_error("[ROSAgent] Parameter does not exist. ")
            capture_exception(KeyError)
            raise


def main():
    # mqtt logging active
    logging.basicConfig(level=logging.DEBUG)
    try:
        ################################################
        # SETUP
        node = InitiationNode()
        agent = MeiliAgent(node)
        handlers = SDKHandlers(agent)

        agent.connection = Connection(
            node, handlers,
            agent.vehicle_list,
        )

        ################################################
        # ROS1 SUBSCRIBERS AND PUBLISHERS
        pub_and_sub(node, agent)

        node.log_info("[ROSAgentNode] Meili Agent is ready for receiving tasks")

        conn_rate = rospy.Rate(0.1)
        pub_rate = rospy.Rate(node.publish_frequency)
        check_threads_rate = rospy.Rate(1)
        agent.run(conn_rate, pub_rate, check_threads_rate)
        rospy.spin()
    except Exception as e:
        log_info(f"[ROSAgent] Error in the main run: {e}")


def exit_gracefully(_, __):
    """Exiting gracefully the program"""
    sys.exit(0)


if __name__ == "__main__":
    try:
        signal.signal(signal.SIGINT, exit_gracefully)
        main()
    except Exception as e:
        capture_exception(e)
