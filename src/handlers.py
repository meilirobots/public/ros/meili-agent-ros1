""" Handlers for the incoming info from Meili SDK"""
import json
import os
# !/usr/bin/env python3


# ROS1 dependencies
import threading

import yaml

import actionlib
import requests
import rospy
from std_msgs.msg import Bool, String
from nav_msgs.srv import LoadMap

# Meili agent dependencies
from meili_agent.msg import DockingGoal
from parse_data import parse_goal, parse_cancel_goal, parse_topic, parse_speed_limit, parse_xy_tolerance, parse_yaw_tolerance, parse_initial_pose
import meili_agent.msg

# Meili Lib dependencies
from meili_ros_lib.sdk_handlers import Handlers
from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.parse_data import goal_setup, rotation_setup, parse_vel, goal_setup_v2
import time
import math

initialize_sentry()


def create_directory_if_not_exists(directory_name):
    home = os.path.expanduser("~")
    directory_path = os.path.join(home, directory_name)

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    return directory_path


def dict_to_yaml(data_dict, file_path):
    with open(file_path, 'w') as file:
        yaml.dump(data_dict, file)


class SDKHandlers(Handlers):
    """Class for all the handlers of messages coming from SDK"""

    def __init__(self, agent):
        Handlers.__init__(self, agent)
        self.rate = rospy.Rate(1)

        # ROS1
        self.slow_down = []
        self.pause_task = []
        for i in range(0, self.agent.number_of_vehicles):
            self.slow_down.append(False)
            self.pause_task.append(False)

    def send_goal(self, data, vehicle_position):
        """Sends Goal information to specific vehicle"""
        self.agent.log_info(f"TAsk received: {data}")
        try:
            rate = rospy.Rate(1)
            x_meters, y_meters, rotation_quaternion = goal_setup(data)
            goal = parse_goal(x_meters, y_meters, rotation_quaternion)
            if self.agent.task_started[vehicle_position] == 0:
                # ROS1
                self.agent.goal_publisher[vehicle_position].publish(goal)
                rate.sleep()
        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Send Goal Error >> {e}")

    def send_goal_v2(self, data, vehicle_position):
        """Sends Goal information to specific vehicle"""
        try:
            rate = rospy.Rate(1)
            x_meters, y_meters, rotation_quaternion = goal_setup_v2(data)
            goal = parse_goal(x_meters, y_meters, rotation_quaternion)
            if self.agent.task_started[vehicle_position] == 0:
                # ROS1
                self.agent.goal_publisher[vehicle_position].publish(goal)
                rate.sleep()
        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Send Goal Error >> {e}")

    def send_waypoints(self, pose, vehicle_position):
        x_meters = [item[0] for item in pose]

        # ROS1
        y_meters = [item[1] for item in pose]
        try:
            rotation = [item[2] for item in pose]
        except IndexError:
            rotation = [0 for _ in pose]
        try:
            max_speed = [item[3] for item in pose]
        except IndexError:
            max_speed = [-1 for _ in pose]

        x = threading.Thread(target=self.waypoints_thread,
                             args=(x_meters, y_meters, rotation, max_speed, vehicle_position,))
        x.start()

        self.agent.log_info(
            f"[ROSHandler] Received waypoints task assigned to vehicle: {self.agent.vehicle_names[vehicle_position]}"
        )

    # ROS1
    def waypoints_thread(self, x_meters, y_meters, rotation, max_speed, vehicle_position):
        rate = rospy.Rate(0.5)
        number_of_waypoints = len(x_meters)
        count = 0
        while count < number_of_waypoints:
            

            if self.agent.task_started[vehicle_position] == 0:

                # Set xy and yaw tolerance for intermediate waypoints
                if count < (number_of_waypoints-1): 
                    # Define tolerance to avoid robot getting stuck when another robots are placed on top of an intermediate waypoint 
                    # and this intermediate waypoint cannot be reached
                    self.set_xy_tolerance(xy_tolerance=self.agent.node.xy_intermediate_wayp_tolerance, vehicle=vehicle_position)
                    self.set_yaw_tolerance(yaw_tolerance=self.agent.node.yaw_intermediate_wayp_tolerance, vehicle=vehicle_position)
                else: 
                    # Default values
                    self.set_xy_tolerance(xy_tolerance=self.agent.node.xy_final_wayp_tolerance, vehicle=vehicle_position)
                    self.set_yaw_tolerance(yaw_tolerance=self.agent.node.yaw_final_wayp_tolerance, vehicle=vehicle_position)

                '''self.agent.log_info(
                    f"[ROSHandler] Number of received waypoints is: {count + 1}/{number_of_waypoints} for {self.agent.vehicle_names[vehicle_position]} ")'''

                if self.agent.pause_paths[vehicle_position]:  # Wait until first waypoint to send FMS the path
                    if count > 0:
                        self.agent.pause_paths[vehicle_position] = False
       
                rotation_quaternion = rotation_setup(rotation[count])
                goal = parse_goal(x_meters[count], y_meters[count], rotation_quaternion)
                self.agent.goal_publisher[vehicle_position].publish(goal)
                if float(max_speed[count]) > 0:
                    self.agent.log_info(
                                        f"[ROSHandler] {self.agent.vehicle_names[vehicle_position]} Setting up navigation max_vel_x: {max_speed[count]}"
                                        )
                    self.set_speed_limit(max_speed[count], vehicle_position)
                else:
                    self.set_speed_limit(self.agent.max_vel_x[vehicle_position], vehicle_position)
                #self.agent.number_of_tasks += count
                count += 1
                time.sleep(0.01)  # Some delay to not overload the process

            if not self.agent.waypoints[vehicle_position]:
                break
                
            time.sleep(0.001)

        self.agent.waypoints[vehicle_position] = False

    def cancel_goal(self, goal_id, vehicle_position):
        # ROS1
        rate = rospy.Rate(1)
        goal_cancel = parse_cancel_goal(goal_id)
        self.agent.goal_canceller[vehicle_position].publish(goal_cancel)
        # self.agent.log_info("[ROSHandler] Waiting to cancel")
        while self.agent.task_started[vehicle_position] == 1:
            rate.sleep()
        self.agent.log_info("[ROSHandler] Goal Canceled")

    def cancel_goal_waypoints(self, goal_id, vehicle_position):
        # ROS1
        rate = rospy.Rate(1)
        goal_cancel = parse_cancel_goal(goal_id)
        self.agent.goal_canceller[vehicle_position].publish(goal_cancel)
        # self.agent.log_info("[ROSHandler] Waiting Waypoints to cancel")
        while self.agent.task_started[vehicle_position] == 1:
            rate.sleep()
        self.agent.log_info("[ROSHandler] Goal Waypoints Canceled")
        self.agent.waypoints[vehicle_position] = False

    # ROS1
    def velocity_publisher_slow_down(self, vel, vehicle_position):
        """ Function to stop the vehicle when collision detected by Traffic Control"""
        frequency = 50
        rate = rospy.Rate(frequency)
        self.agent.log_info(f"[ROSHandler] Stopping vehicle: {self.agent.vehicle_names[vehicle_position]}")
        while self.slow_down[vehicle_position]:
            self.agent.cmd_vel_publisher[vehicle_position].publish(vel)
            rate.sleep()
    
    def velocity_publisher_pause_task(self, vel, vehicle_position):
        """ Function to stop the vehicle when Pause Task received"""
        frequency = 50
        rate = rospy.Rate(frequency)
        self.agent.log_info(f"[ROSHandler] Pausing Task from vehicle: {self.agent.vehicle_names[vehicle_position]}")
        while self.pause_task[vehicle_position]:
            self.agent.cmd_vel_publisher[vehicle_position].publish(vel)
            rate.sleep()

    def slow_down_handler(self, slow_down_msg, data: dict, vehicle: str):
        """Handles slowdown messages and sends to specific vehicle parameters"""
        if self.agent.node.traffic_control:
            max_vel_x = slow_down_msg.max_vel_x
            max_vel_theta = slow_down_msg.max_vel_theta

            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            self.agent.log_info(
                f"[ROSHandler] Collision situation detected for vehicle: {self.agent.vehicle_names[vehicle_position]} ")

            # ROS 1
            vel = parse_vel(max_vel_x)
            x = threading.Thread(target=self.velocity_publisher_slow_down, args=(vel, vehicle_position), daemon=True)
            threads = []
            for index in range(self.agent.number_of_vehicles):
                threads.append(x)
            if not self.slow_down[vehicle_position]:
                self.slow_down[vehicle_position] = True
                threads[vehicle_position].start()

    def collision_clearance_handler(self, collision_clearance_msg, data: dict, vehicle: str):
        """Handles clear collision messages and sends to specific vehicle parameters"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        msg_type = collision_clearance_msg.message_type
        if msg_type == "slow_down":
            self.agent.log_info(f"[ROSHandler] Restarting vehicle: {self.agent.vehicle_names[vehicle_position]}")

            # ROS1
            self.slow_down[vehicle_position] = False

        # elif msg_type == "path_rerouting":
        #    path_data = {"path": None, "rotation_angles": None}
        #    vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        #    self.agent.log_info(
        #        f"[ROSHandler] Path Rerouting Clearance for vehicle: {self.agent.vehicle_names[vehicle_position]}")
        #    path_data["rotation_angles"] = [data["data"]["task_data"]["location"]["rotation"]] # old structure >> not use
        #    location = collision_clearance_msg.task_data.location # not sure if this will be the structure
        #    path_data["rotation_angles"] = [location.rotation]
        #    metric = location.metric
        #    path_data["path"] = [[metric["x"], metric["y"]]]
        #    self.path_rerouting_handler(_, path_data, vehicle)

    def docking_routine_request_handler(self, data: dict, vehicle: str):
        self.agent.log_info("[ROSHandler] Docking Routine RECORDING request")
        try:
            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            vehicle = self.agent.vehicles[vehicle_position]
            vehicle_prefix = str(vehicle["prefix"])

            if vehicle_prefix == "None":
                vehicle_prefix = ""

            # ROS1
            self.client[vehicle_position] = actionlib.SimpleActionClient("docking", meili_agent.msg.DockingAction)
            self.agent.log_info("[ROSHandler] Waiting for docking server")
            self.client[vehicle_position].wait_for_server()

            goal = DockingGoal()
            goal.vehicle_position = vehicle_position
            goal.frequency = 30
            string = String()
            string.data = vehicle_prefix
            goal.vehicle_prefix = string
            self.agent.log_info("Before sending goal")
            self.client[vehicle_position].send_goal(goal)

        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Error on Docking Routine Request: {e}")

    def download_image(self, url, file_path):
        response = requests.get(url, stream=True, timeout=5)
        if response.status_code == 200:
            with open(file_path, 'wb') as file:
                for chunk in response.iter_content(1024):
                    file.write(chunk)
            self.agent.log_info(f"[API_Handler] Map image downloaded and saved as {file_path}")
        else:
            self.agent.log_info(f"[API_Handler] Failed to download  map image. Response: {response}")

    def get_new_map_png(self, data):
        displayable_image = data.displayable_image
        file_name = "/map"
        directory = create_directory_if_not_exists("maps")
        self.download_image(displayable_image, directory + file_name + ".png")

    def get_new_map_yaml(self, data):
        yaml_file = data.yaml_file
        file_name = "/map"
        directory = create_directory_if_not_exists("maps")

        map_dict = {}
        image = directory + file_name +".png"
        map_dict["image"] = image
        map_dict["resolution"] = float(yaml_file.get("resolution", 0.1))
        map_dict["mode"] = yaml_file.get("mode", "trinary")
        origin = yaml_file.get("origin", [0.0, 0.0, 0.0])

        map_dict["origin"] = [float(i) for i in origin]
        map_dict["negate"] = int(yaml_file.get("negate", 0))
        map_dict["occupied_thresh"] = float(yaml_file.get("occupied_thresh", 0.65))
        map_dict["free_thresh"] = yaml_file.get("free_thresh", 0.196)
        if map_dict["free_thresh"] is None:
            map_dict["free_thresh"] = 0.196

        dict_to_yaml(map_dict, directory + file_name + ".yaml")

        return directory + file_name + ".yaml"

    def update_map_handler(self, data: dict, vehicle: str, status):
        self.agent.log_info("[ROSHandler] Received msg in map update handler")
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        vehicle = self.agent.vehicles[vehicle_position]
        vehicle_prefix = str(vehicle["prefix"])

        if vehicle_prefix == "None":
            vehicle_prefix = ""

        change_map_service = vehicle_prefix + "/change_map"
        #if change_map does not have prefix you can uncoment next line
        #change_map_service ="/change_map"
        rospy.wait_for_service(change_map_service)
        self.agent.log_info(f"[ROSHandler] Map Update Handler {change_map_service}")
        self.get_new_map_png(data)
        map_url = self.get_new_map_yaml(data)

        try:
            change_map = rospy.ServiceProxy(change_map_service, LoadMap)
            resp = change_map(map_url)
            result = resp.result
        except rospy.ServiceException as e:
            self.agent.log_error(f"[ROSHandler] Change Map Service call failed: {e}")

        if result == 0:
            self.agent.log_info("[ROSHandler] Change Map Service: SUCCESS")
        elif result == 1:
            self.agent.log_info("[ROSHandler] Change Map Service: MAP DOES NOT EXISTS")
        elif result == 2:
            self.agent.log_info("[ROSHandler] Change Map Service: INVALID MAP DATA")
        elif result == 3:
            self.agent.log_info("[ROSHandler] Change Map Service: INVALID MAP METADATA")
        elif result == 255:
            self.agent.log_info("[ROSHandler] Change Map Service: UNDEFINED FAILURE")

    def topic_handler(self, _, topics, vehicle):
        """find vehicle token to passed to the topic streaming node"""
        if topics and vehicle:

            # initialise custom msg to send topic info

            for index in topics["data"]:
                self.agent.total_topics += 1
                index["vehicle_uuid"] = vehicle

                msg = parse_topic(index, self.agent)

                self.agent.topic_streaming_talker(msg)
                self.agent.log_info(
                    f"[ROSHandler] Topic to stream {index['topic']}, for vehicle {vehicle}"
                )
        else:
            rospy.loginfo("[ROSHandler] No topics to stream")

    def end_recording_publisher(self, vehicle_prefix):
        pub = rospy.Publisher(
            vehicle_prefix + "/recording", Bool, queue_size=5
        )
        return pub

    def set_speed_limit(self, speed_limit, vehicle):
        speed_limit_msg = parse_speed_limit(speed_limit)
        self.agent.speed_limit_publisher[vehicle].update_configuration(speed_limit_msg)

    def set_xy_tolerance(self, xy_tolerance, vehicle): 
        xy_tolerance_msg = parse_xy_tolerance(xy_tolerance)
        self.agent.xy_tolerance_publisher[vehicle].update_configuration(xy_tolerance_msg)

    def set_yaw_tolerance(self, yaw_tolerance, vehicle): 
        yaw_tolerance_msg = parse_yaw_tolerance(yaw_tolerance)
        self.agent.yaw_tolerance_publisher[vehicle].update_configuration(yaw_tolerance_msg)

    def update_vehicle_settings(self, frequency, vehicle: str):
        self.agent.log_info(
                    f"[ROSHandler] Publish frequency changed to {float(frequency)}, for vehicle {vehicle}"
                )
        self.agent.publish_frequency=float(frequency)
        self.agent.publish_frequency_updated = True
        
    def pause_task_handler(self, pause_task_msg, vehicle: str):
        """ Publish 0 vel when a vehicle task is paused"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)

        self.agent.log_info(
                    f"[ROSHandler] Pausing task of vehicle {self.agent.vehicle_names[vehicle_position]}"
                )
        
        pause_vel = parse_vel(0.0)

        vel_pub_thread = threading.Thread(target=self.velocity_publisher_pause_task, args=(pause_vel, vehicle_position), daemon=True)

        if not self.pause_task[vehicle_position]:
            self.pause_task[vehicle_position] = True
            vel_pub_thread.start()
    
    def resume_task_handler(self, pause_task_msg, vehicle: str):
        """ Stop publishing 0 vel when a vehicle task is resumed"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)

        self.agent.log_info(
                    f"[ROSHandler] Resuming task of vehicle {self.agent.vehicle_names[vehicle_position]}"
                )
        
        self.pause_task[vehicle_position] = False
    
    
    def remove_vehicle_from_fleet_handler(self, vehicle: str):
        """ Remove vehicle from fleet """
        self.agent.log_info(f"[ROSHandler] Removing vehicle {vehicle} from fleet")
        self.agent.remove_vehicle_from_fleet(vehicle)

    def set_initial_position_handler(self, vehicle: str, pose_msg: dict):
        """
        Set estimated pose for a vehicle, pose must
        be a dictionary with three keys: {"x", "y", "yaw"}
        """
        rate = rospy.Rate(1)
        self.agent.log_info(f"[ROSHandler] Setting estimated pose for {vehicle}")
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        data={
            "metric":
                {
                "x": pose_msg["x"],
                "y": pose_msg["y"]
                },
            "rotation": math.degrees(float(pose_msg["yaw"]))
        }

        x_meters, y_meters, rotation_quaternion = goal_setup(data)
        initial_pose = parse_initial_pose(x_meters, y_meters, rotation_quaternion)
        self.agent.initial_pose_publisher[vehicle_position].publish(initial_pose)
        rate.sleep()
        self.agent.log_info(f"[ROSHandler] Initial pose set for {initial_pose}")