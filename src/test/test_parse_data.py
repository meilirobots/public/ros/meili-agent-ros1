#!/usr/bin/env python3
# standart python3 libraries
import sys
from unittest import TestCase

import rospy
import rostest

from parse_data import parse_goal


class ParseDataTestCase(TestCase):
    def setUp(self):
        rospy.init_node("agent")

    def test_parse_battery_data(self):
        pass

    def test_parse_battery_data_wrong_data(self):
        pass

    def test_parse_goal(self):
        x = 1
        y = 2
        r_t_q = {
            "qx": 0.0,
            "qy": 0.8509035245341184,
            "qz": 0.0,
            "qw": 0.5253219888177297,
        }

        goal = parse_goal(x, y, r_t_q)
        self.assertEqual(goal.header.frame_id, "map")
        self.assertEqual(goal.pose.position.x, 1.0)
        self.assertEqual(goal.pose.position.y, 2.0)
        self.assertEqual(goal.pose.position.z, 0.0)
        self.assertEqual(goal.pose.orientation.x, 0.0)
        self.assertEqual(goal.pose.orientation.y, 0.8509035245341184)
        self.assertEqual(goal.pose.orientation.z, 0.0)
        self.assertEqual(goal.pose.orientation.w, 0.5253219888177297)

    def test_parse_goal_wrong_data(self):
        x = "a"
        y = 2
        r_t_q = {
            "qx": 0.0,
            "qy": 0.8509035245341184,
            "qz": 0.0,
            "qw": 0.5253219888177297,
        }

        with self.assertRaises(ValueError):
            goal = parse_goal(x, y, r_t_q)

        r_t_q = {
            "qx": "123",
            "qy": 0.8509035245341184,
            "qz": 0.0,
            "qw": 0.5253219888177297,
        }

        with self.assertRaises(ValueError):
            goal = parse_goal(x, y, r_t_q)


if __name__ == "__main__":
    rostest.rosrun("meili_agent", "test_parse_data", ParseDataTestCase, sys.argv)
