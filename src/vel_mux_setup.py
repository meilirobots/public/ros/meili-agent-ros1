import yaml
import os

def generate_robot_launch_code(filename, n_robots):
    template_start = """<launch>
  <arg name="cmd_vel_out" default="/cmd_vel"/>
  <arg name="config_locks" default="$(find twist_mux)/config/twist_mux_locks.yaml"/>"""
    
    template_load_configs= """
  <arg name="prefix_robot{i}" default="/robot{i}"/>
  <arg name="config_topics_robot{i}" default="$(find twist_mux)/config/twist_mux_topics_robot{i}.yaml"/>"""
    
    template_nodes= """
  <node pkg="twist_mux" type="twist_mux" name="twist_mux_robot{i}" output="screen">
    <remap from="cmd_vel_out" to="$(arg prefix_robot{i})$(arg cmd_vel_out)"/>
    <rosparam file="$(arg config_locks)" command="load"/>
    <rosparam file="$(arg config_topics_robot{i})" command="load"/>
  </node>"""


    template_end = """
</launch>
    """
    with open(f"{filename}", "w") as f:
        f.write(template_start)
        # Write config files loading commands
        for i in range(n_robots):
            load_configs_code = template_load_configs.format(i=i+1)
            f.write(load_configs_code)
        f.write('\n\n')
        # Write launch node commands
        for i in range(n_robots):
            nodes_code = template_nodes.format(i=i+1)
            f.write(nodes_code)
            f.write('\n\n')
        f.write(template_end)

# Get meili-agent package directory
meili_agent_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
ws_src_path = os.path.dirname(meili_agent_path)

# Get meili-agent config yaml path
yaml_file_path = meili_agent_path + '/config/config.yaml'

# Open the YAML file and load its contents
with open(yaml_file_path, 'r') as file:
    meili_config = yaml.safe_load(file)

# Get number of robots in the fleet
n_robots = len(meili_config['vehicles'])

###
### CREATE VEL MUX CONFIG FILES FOR EACH ROBOT
###

# Create velocity multiplexor config file 
vel_mux_config = []
vel_mux_file_name = []

for i in range(n_robots): 
    # Create config file for each robot
    vel_mux_config.append({'topics': []})

    # Get robot prefix
    robot_prefix = meili_config['vehicles'][i]['prefix']

    # Fill config file fields
    vel_mux_config[i]['topics'].append({
                                    'name': 'navigation_' + robot_prefix[1:],
                                    'topic': robot_prefix + '/cmd_vel_nav',
                                    'timeout': 0.1,
                                    'priority': 1
                                    })
    
    vel_mux_config[i]['topics'].append({
                                    'name': 'meili_' + robot_prefix[1:],
                                    'topic': robot_prefix + '/cmd_vel_meili',
                                    'timeout': 0.1,
                                    'priority': 255
                                    })

    # Define the config file path
    vel_mux_file_name.append('twist_mux_topics_' + robot_prefix[1:] + '.yaml')
    vel_mux_config_path = ws_src_path + '/twist_mux/config/' + vel_mux_file_name[-1]
    
    # Create the config file
    with open(vel_mux_config_path, 'w') as file:
        yaml.dump(vel_mux_config[i], file, default_flow_style=False)

###
### CREATE VEL MUX LAUNCH FILE 
###

# Save the XML to a file with indentation
vel_mux_launch_path = ws_src_path + '/twist_mux/launch/twist_mux.launch'
generate_robot_launch_code(vel_mux_launch_path, n_robots)

