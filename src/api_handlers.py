import json
import os

from meili_sdk.clients.apitoken_client import APITokenClient
from meili_sdk.clients import get_client

import requests
import yaml


def create_directory_if_not_exists(directory_name):
    home = os.path.expanduser("~")
    directory_path = os.path.join(home, directory_name)

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    return directory_path

def dict_to_yaml(data_dict, file_path):
    with open (file_path, 'w') as file:
        yaml.dump(data_dict, file)

def bytes_to_dict(byte_data):
    string_data = byte_data.decode('utf-8')  # Decode bytes into a string
    dict_data = json.loads(string_data)  # Parse the string as a dictionary
    return dict_data


def get_meili_credentials():
    data = {
        "email": "nuria@meilirobots.com",
        "password": "password"}
    payload = json.dumps(data)
    return payload

def get_authorization():
    url = "https://development.meilirobots.com/api/auth/login/"
    payload = get_meili_credentials()
    response = requests.post(url, headers={"Content-Type": "application/json"}, data=payload, timeout=5)
    response = json.loads(response.text)

    authorization = response["token"]
    return authorization


class APIHandlers:

    def __init__(self, log_info):
        self.api_client = None
        self.log_info = log_info

    def api_setup(self):
        """api-setup"""
        token = get_authorization()
        self.api_client = get_client(token)

    def update_map(self,team_name):
        team = self.get_team_info(team_name)
        self.get_new_map_png(team)
        self.get_new_map_yaml(team)

    def get_team_info(self, slug):
        teams = self.api_client.get_team_resources().get_teams()
        res = json.loads(teams[2])
        index = next((i for i, item in enumerate(res) if item["slug"] == slug), None)
        team = teams[1][index]
        return team

    def get_new_map_png(self, team, ):
        area_map = self.api_client.get_team_resources().get_indoor_area_map(team)
        displayable_image = area_map[1]["displayable_image"]
        file_name = "/map"
        directory = create_directory_if_not_exists("maps")
        self.download_image(displayable_image, directory + file_name + ".png")

    def get_new_map_yaml(self,team):
        indoor_area = self.api_client.get_team_resources().get_indoor_team_area(team)
        yaml_file = bytes_to_dict(indoor_area[2])
        file_name = "/map"
        directory = create_directory_if_not_exists("maps")

        map_dict = {}
        map_dict["image"] = directory
        map_dict["resolution"] = yaml_file.get("resolution", 0.1)
        map_dict["mode"] = yaml_file.get("mode", "trinary")
        map_dict["origin"] = yaml_file.get("origin", [0.0, 0.0, 0.0])
        map_dict["negate"] = int(yaml_file.get("negate", 0))
        map_dict["occupied_thresh"] = yaml_file.get("occupied_thresh", 0.65)
        map_dict["free_thresh"] = yaml_file.get("free_thresh", 0.196)

        dict_to_yaml(map_dict, directory + file_name+ ".yaml")

    def download_image(self, url, file_path):
        response = requests.get(url, stream=True, timeout=5)
        if response.status_code == 200:
            with open(file_path, 'wb') as file:
                for chunk in response.iter_content(1024):
                    file.write(chunk)
            self.log_info(f"[API_Handler] Map image downloaded and saved as {file_path}")
        else:
            self.log_info("[API_Handler] Failed to download  map image.")